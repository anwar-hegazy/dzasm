/* -------------------------------------------------------------------------------------------------

    DDDDDDDDDDDDD            ZZZZZZZZZZZZZZZZ
    DDDDDDDDDDDDDDD        ZZZZZZZZZZZZZZZZ
    DDDD         DDDD               ZZZZZ
    DDDD         DDDD             ZZZZZ
    DDDD         DDDD           ZZZZZ             AAAAAA         SSSSSSSSSSS   MMMM       MMMM
    DDDD         DDDD         ZZZZZ              AAAAAAAA      SSSS            MMMMMM   MMMMMM
    DDDD         DDDD       ZZZZZ               AAAA  AAAA     SSSSSSSSSSS     MMMMMMMMMMMMMMM
    DDDD         DDDD     ZZZZZ                AAAAAAAAAAAA      SSSSSSSSSSS   MMMM MMMMM MMMM
    DDDDDDDDDDDDDDD     ZZZZZZZZZZZZZZZZZ     AAAA      AAAA           SSSSS   MMMM       MMMM
    DDDDDDDDDDDDD     ZZZZZZZZZZZZZZZZZ      AAAA        AAAA  SSSSSSSSSSS     MMMM       MMMM

    (C) Copyright Gunther Strube (gstrube@gmail.com), 1996-2016

    This file is part of DZasm.

    DZasm is free software; you can redistribute it and/or modify it under the terms of the
    GNU General Public License as published by the Free Software Foundation;
    either version 2, or (at your option) any later version.
    DZasm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License along with DZasm;
    see the file COPYING. If not, write to the
    Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

 -------------------------------------------------------------------------------------------------*/


enum atype              {
				vacuum, program, addrtable, defp, defw, defb, defs, string, nop,
				romhdr, frontdor, appldor, helpdor, mthtpc, mthcmd, mthhlp, mthtkn,
				notfound
			};
                        	/* NB: remember to update gAreaTypes table in 'areas.c' */
enum files              {
				none, stdio, fileio, director, memory, dor, syspar, saverestore,
              	                floatp, integer, serinterface, screen, timedate, chars, error, map,
              	                alarm, filter, tokens, intrrupt, printer, handle
                        };

enum symbols		{
				space, strconq, dquote, squote, semicolon, comma, fullstop, lparen, lcurly, rcurly, rparen,
				plus, minus, multiply, divi, mod, power, assign, bin_and, bin_or, bin_xor, less,
                greater, log_not, constantexpr, newline, lessequal, greatequal, notequal, name, number,
				decmconst, hexconst, binconst, charconst, negated, nil
			};

enum truefalse          { false, true };

typedef struct area {
        int  			start;
        int  			end;
        enum atype     	areatype;
        enum truefalse  parsed;
        struct area     *prevarea;
        struct area     *nextarea;
        void 			*attributes;		/* Pointer to area specific attributes */
} DZarea;

typedef struct mthinfo {
        int  		dorAddress;			/* DOR base address for this MTH area */
        int			mthHelp;			/* MTH Help pointer for this MTH area, or 0 if not defined */
} MthPointers;

struct  PrsAddrStack {
        int         labeladdr;
        struct PrsAddrStack *previtem;
};

typedef struct address {
    int			addr;               /* the recorded parsing address */
    int         visited;            /* number of times this address has been visited */
} ParsedAddress;

typedef struct Label {
    int			addr;
    char		*name;
        enum truefalse  	referenced;
        enum truefalse  	local;      /* is label inside local area? */
        enum truefalse  	xref;		/* is data reference declared as XREF (external) by user? */
        enum truefalse  	xdef;		/* is data reference declared as XDEF (global) by user? */
        enum truefalse		addrref;	/* is this label a call reference (= true) */
} LabelRef;

typedef struct expr {
    int		    addr;           /* entry point to replace mnemonic constant */
    char		*expr;          /* with ASCII expression (or just a string mnemonic representation) */
} Expression;

typedef struct constant {
    int		    constantval;	/* global constant value of matching mnemonic constants */
    char		*constname;     /* to be replaced with ASCII name */
} GlobalConstant;

typedef struct remline {
    char			*line;		/* pointer to comment line text */
    struct remline	*next;		/* next line of comment in list */
} Commentline;

typedef struct rem {
    int  			addr;		/* address of printing comment */
    char			position;	/* are comments to be printed before ('<') or after ('>') mnemonics? */
    Commentline		*comments;	/* single list of comment lines */
} Remark;

typedef struct incf {
    char			*filename;	/* pointer to explicit include filename */
    struct incf		*next;		/* next include file item */
} IncludeFile;

typedef void 	(*ptrfunc) ();	/* ptr to function returning void */
typedef int 	(*fptr) (const void *, const void *);

struct dzcmd {
    char *cmd;
    ptrfunc dzcmd;
};

#define MAXCODESIZE 65536
